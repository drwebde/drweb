<?php
 /* =============================================================================
 Die functions.php fuer Dein Child-Theme
============================================================================= */
/**
 * Diese Funktion ist zwingend notwendig. Sie installiert das CSS des Eltern-Themes
 * Das CSS Deines Child-Themes wird ohne Dein Zutun automatisch installiert.
 */
/* =============================================================================
Ab hier unterhalb kannst Du Deine Aenderungen einfügen
=============================================================================== */

add_action( 'wp_enqueue_scripts', 'tb_theme_enqueue_styles' );
function tb_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

function twentytwenty_read_more_tag( $html ) {
	return preg_replace( '/<a(.*)>(.*)<\/a>/iU', sprintf( '<div class="read-more-button-wrap"><a$1><span class="faux-button">$2</span> <span class="screen-reader-text">"%1$s"</span></a></div>', get_the_title( get_the_ID() ) ), $html );
}

add_filter( 'the_content_more_link', 'twentytwenty_read_more_tag' );